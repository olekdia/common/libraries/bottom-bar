package com.olekdia.sample

import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity
import com.olekdia.bottombar.BottomBar

class SampleActivity : AppCompatActivity() {

    private var bottomBar: BottomBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_sample)

        bottomBar = findViewById(R.id.bottom_bar)

        bottomBar?.apply {
            setItems(R.xml.bb_tabs)
            getTabWithId(R.id.task_button)?.badgeLabel = "5"
        }
    }
}
