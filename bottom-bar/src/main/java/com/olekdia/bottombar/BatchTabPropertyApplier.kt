package com.olekdia.bottombar

internal class BatchTabPropertyApplier(private val bottomBar: BottomBar) {

    internal interface TabPropertyUpdater {
        fun update(tab: BottomBarTab)
    }

    fun applyToAllTabs(propertyUpdater: TabPropertyUpdater) {
        val tabCount: Int = bottomBar.tabCount

        if (tabCount > 0) {
            for (i in 0 until tabCount) {
                bottomBar.getTabAtPosition(i)?.let {
                    propertyUpdater.update(it)
                }
            }
        }
    }
}