package com.olekdia.bottombar

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.os.Parcelable
import android.view.Gravity
import android.view.PointerIcon
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.IdRes
import androidx.annotation.VisibleForTesting
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.view.ViewCompat
import com.olekdia.androidcommon.extensions.backgroundCompat
import com.olekdia.androidcommon.extensions.resolveDrawable
import com.olekdia.androidcommon.extensions.setTextAppearanceCompat
import com.olekdia.common.extensions.ifNotNull

/*
 * BottomBar library for Android
 * Copyright (c) 2016 Iiro Krankka (http://github.com/roughike).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class BottomBarTab internal constructor(context: Context) : LinearLayout(context) {

    private val iconShiftingActiveTopPadding: Int
    private val iconFixedTopPadding: Int
    private val iconShiftingInactiveTopPadding: Int

    @VisibleForTesting
    internal var badge: BottomBarBadge? = null

    internal var type = Type.FIXED
    internal var isTitleless: Boolean = false
        set(isTitleless) {
            if (isTitleless && iconResId == 0) {
                throw IllegalStateException(
                    "This tab is supposed to be " +
                            "icon only, yet it has no icon specified. Index in " +
                            "container: " + indexInTabContainer
                )
            }

            field = isTitleless
        }
    var iconResId: Int = 0
        set(iconResId) {
            field = iconResId
            updateIcon()
        }
    var title: String? = null
        set(title) {
            field = title
            updateTitle()
        }
    var inActiveAlpha: Float = 0F
        set(inActiveAlpha) {
            field = inActiveAlpha

            if (!isActive) {
                setAlphas(inActiveAlpha)
            }
        }
    var activeAlpha: Float = 0F
        set(activeAlpha) {
            field = activeAlpha

            if (isActive) {
                setAlphas(activeAlpha)
            }
        }
    var inActiveColor: Int = 0
        set(inActiveColor) {
            field = inActiveColor

            if (!isActive) {
                setColors(inActiveColor)
            }
        }
    var activeColor: Int = 0
        set(activeIconColor) {
            field = activeIconColor

            if (isActive) {
                setColors(this.activeColor)
            }
        }
    var barColorWhenSelected: Int = 0
    var badgeBackgroundColor: Int = 0
        set(badgeBackgroundColor) {
            field = badgeBackgroundColor

            if (badge != null) {
                badge!!.setColoredCircleBackground(badgeBackgroundColor)
            }
        }
    var badgeFontColor: Int = 0
        set(color) {
            field = color
            badge?.setFontColor(color)
        }
    var badgeHidesWhenActive: Boolean = false

    var badgeLabel: String?
        get() = badge?.let { if (it.isVisible) it.label else null }
        set(value) {
            if (value == null || value.isEmpty()) {
                badge?.removeFromTab(this)
                badge = null

                return
            }

            if (badge == null) {
                badge = BottomBarBadge(context).also {
                    it.attachToTab(this, this.badgeBackgroundColor, this.badgeFontColor)
                }
            }

            badge?.label = value

            if (isActive && badgeHidesWhenActive) {
                badge?.hide()
            }
        }

    internal var iconView: AppCompatImageView? = null
        private set
    internal var titleView: TextView? = null
        private set
    internal var isActive: Boolean = false
        private set
    var indexInTabContainer: Int = 0
    var titleTextAppearance: Int = 0
        internal set(resId) {
            field = resId
            updateCustomTextAppearance()
        }
    var titleTypeFace: Typeface? = null
        private set

    internal val layoutResource: Int
        @VisibleForTesting
        get() {
            return when (type) {
                Type.FIXED -> R.layout.bb_bottom_bar_item_fixed
                Type.SHIFTING -> R.layout.bb_bottom_bar_item_shifting
                Type.TABLET -> R.layout.bb_bottom_bar_item_fixed_tablet
            }
        }

    val outerView: ViewGroup?
        get() = parent as? ViewGroup

    internal val currentDisplayedIconColor: Int
        get() = iconView?.getTag(R.id.bb_bottom_bar_color_id) as? Int ?: 0

    internal val currentDisplayedTitleColor: Int
        get() = titleView?.currentTextColor ?: 0

    internal val currentDisplayedTextAppearance: Int
        get() = titleView?.getTag(R.id.bb_bottom_bar_appearance_id) as? Int ?: 0

    init {
        val res = context.resources
        iconShiftingActiveTopPadding =
            res.getDimensionPixelSize(R.dimen.bb_icon_shifting_active_top_padding)
        iconShiftingInactiveTopPadding =
            res.getDimensionPixelSize(R.dimen.bb_icon_shifting_inactive_top_padding)
        iconFixedTopPadding = res.getDimensionPixelSize(R.dimen.bb_icon_fixed_top_padding)

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            pointerIcon = PointerIcon.getSystemIcon(context, PointerIcon.TYPE_HAND)
        }
    }

    constructor(
        context: Context,
        config: Config,
        indexInContainer: Int,
        @IdRes id: Int,
        title: String,
        iconResId: Int
    ) : this(context) {
        setConfig(config)
        indexInTabContainer = indexInContainer
        setId(id)
        this.title = title
        this.iconResId = iconResId
    }

    fun setConfig(config: Config) {
        inActiveAlpha = config.inActiveTabAlpha
        activeAlpha = config.activeTabAlpha
        inActiveColor = config.inActiveTabColor
        activeColor = config.activeTabColor
        barColorWhenSelected = config.barColorWhenSelected
        badgeBackgroundColor = config.badgeBackgroundColor
        badgeFontColor = config.badgeFontColor
        badgeHidesWhenActive = config.badgeHidesWhenSelected
        titleTextAppearance = config.titleTextAppearance

        setTitleTypeface(config.titleTypeFace)
    }

    internal fun prepareLayout() {
        View.inflate(context, layoutResource, this)

        orientation = VERTICAL
        gravity = if (isTitleless) Gravity.CENTER else Gravity.CENTER_HORIZONTAL

        val params = LayoutParams(
            if (type == Type.TABLET) LayoutParams.MATCH_PARENT else LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT
        )

        if (type == Type.TABLET) {
            params.gravity = Gravity.CENTER_HORIZONTAL
        }
        layoutParams = params

        backgroundCompat = context.resolveDrawable(R.attr.selectableItemBackgroundBorderless)

        iconView = findViewById(R.id.bb_bottom_bar_icon)
        iconView?.setImageResource(this.iconResId)

        if (type != Type.TABLET && !isTitleless) {
            titleView = findViewById(R.id.bb_bottom_bar_title)
            titleView?.visibility = View.VISIBLE

            if (type == Type.SHIFTING) {
                findViewById<View>(R.id.spacer).visibility = View.VISIBLE
            }

            updateTitle()
        }

        updateCustomTextAppearance()
        updateCustomTypeface()
    }

    private fun updateTitle() {
        titleView?.text = title
    }

    private fun updateIcon() {
        iconView?.setImageResource(iconResId)
    }

    private fun updateCustomTextAppearance() {
        val title: TextView = titleView ?: return
        if (titleTextAppearance == 0) return

        title.setTextAppearanceCompat(titleTextAppearance)

        title.setTag(R.id.bb_bottom_bar_appearance_id, titleTextAppearance)
    }

    private fun updateCustomTypeface() {
        ifNotNull(
            titleTypeFace,
            titleView
        ) { typeFace, view ->
            view.typeface = typeFace
        }
    }

    fun removeBadge() {
        badgeLabel = null
    }

    internal fun hasActiveBadge(): Boolean = badge != null

    internal fun setIconTint(tint: Int) {
        iconView?.setColorFilter(tint)
    }

    fun setTitleTypeface(typeface: Typeface?) {
        titleTypeFace = typeface
        updateCustomTypeface()
    }

    internal fun select(animate: Boolean) {
        isActive = true

        if (animate) {
            animateIcon(activeAlpha, ACTIVE_SHIFTING_TITLELESS_ICON_SCALE)
            animateTitle(iconShiftingActiveTopPadding, ACTIVE_TITLE_SCALE, activeAlpha)
            animateColors(inActiveColor, activeColor)
        } else {
            setTitleScale(ACTIVE_TITLE_SCALE)
            setTopPadding(iconShiftingActiveTopPadding)
            setIconScale(ACTIVE_SHIFTING_TITLELESS_ICON_SCALE)
            setColors(activeColor)
            setAlphas(activeAlpha)
        }

        isSelected = true

        if (badgeHidesWhenActive) {
            badge?.hide()
        }
    }

    internal fun deselect(animate: Boolean) {
        isActive = false

        val isShifting: Boolean = type == Type.SHIFTING

        val titleScale: Float = if (isShifting) 0F else INACTIVE_FIXED_TITLE_SCALE
        val iconPaddingTop: Int =
            if (isShifting) iconShiftingInactiveTopPadding else iconFixedTopPadding

        if (animate) {
            animateTitle(iconPaddingTop, titleScale, inActiveAlpha)
            animateIcon(inActiveAlpha, INACTIVE_SHIFTING_TITLELESS_ICON_SCALE)
            animateColors(activeColor, inActiveColor)
        } else {
            setTitleScale(titleScale)
            setTopPadding(iconPaddingTop)
            setIconScale(INACTIVE_SHIFTING_TITLELESS_ICON_SCALE)
            setColors(inActiveColor)
            setAlphas(inActiveAlpha)
        }

        isSelected = false

        badge?.let {
            if (!isShifting && !it.isVisible) {
                it.show()
            }
        }
    }

    private fun animateColors(previousColor: Int, color: Int) {
        ValueAnimator().apply {
            setIntValues(previousColor, color)
            setEvaluator(ArgbEvaluator())
            addUpdateListener { valueAnimator ->
                setColors(valueAnimator.animatedValue as? Int ?: 0)
            }

            duration = 150
            start()
        }
    }

    private fun setColors(color: Int) {
        iconView?.apply {
            setColorFilter(color)
            setTag(R.id.bb_bottom_bar_color_id, color)
        }

        titleView?.setTextColor(color)
    }

    private fun setAlphas(alpha: Float) {
        iconView?.alpha = alpha
        titleView?.alpha = alpha
    }

    internal fun updateWidth(endWidth: Float, animated: Boolean) {
        if (!animated) {
            layoutParams.width = endWidth.toInt()

            badge?.let {
                it.adjustPositionAndSize(this)
                if (!isActive) {
                    it.show()
                }
            }

            return
        }

        val start: Float = width.toFloat()

        val animator: ValueAnimator = ValueAnimator.ofFloat(start, endWidth)

        animator.let {
            it.duration = 150

            it.addUpdateListener { animator ->
                badge?.adjustPositionAndSize(this)

                layoutParams?.apply {
                    width = (animator.animatedValue as? Float ?: 0F).toInt()
                }
            }
        }

        // Workaround to avoid using faulty onAnimationEnd() listener
        postDelayed({
            badge?.let {
                if (!isActive) clearAnimation()
                it.adjustPositionAndSize(this)
                if (!isActive) it.show()
            }
        }, animator.duration)

        animator.start()
    }

    private fun updateBadgePosition() {
        badge?.adjustPositionAndSize(this)
    }

    private fun setTopPaddingAnimated(start: Int, end: Int) {
        if (type == Type.TABLET || isTitleless) return

        ValueAnimator.ofInt(start, end).let {
            it.addUpdateListener { animation ->
                iconView?.apply {
                    setPadding(
                        paddingLeft,
                        animation.animatedValue as? Int ?: 0,
                        paddingRight,
                        paddingBottom
                    )
                }
            }

            it.duration = ANIMATION_DURATION
            it.start()
        }
    }

    private fun animateTitle(padding: Int, scale: Float, alpha: Float) {
        if (type == Type.TABLET && isTitleless) return

        setTopPaddingAnimated(iconView?.paddingTop ?: 0, padding)

        val view: View = titleView ?: return

        ViewCompat
            .animate(view)
            .setDuration(ANIMATION_DURATION)
            .scaleX(scale)
            .scaleY(scale)
            .apply {
                alpha(alpha)
                start()
            }
    }

    private fun animateIconScale(scale: Float) {
        val view: View = iconView ?: return

        ViewCompat
            .animate(view)
            .setDuration(ANIMATION_DURATION)
            .scaleX(scale)
            .scaleY(scale)
            .start()
    }

    private fun animateIcon(alpha: Float, scale: Float) {
        val view: View = iconView ?: return

        ViewCompat
            .animate(view)
            .setDuration(ANIMATION_DURATION)
            .alpha(alpha)
            .start()

        if (isTitleless && type == Type.SHIFTING) {
            animateIconScale(scale)
        }
    }

    private fun setTopPadding(topPadding: Int) {
        if (type == Type.TABLET || isTitleless) return

        iconView?.apply {
            setPadding(
                paddingLeft,
                topPadding,
                paddingRight,
                paddingBottom
            )
        }
    }

    private fun setTitleScale(scale: Float) {
        if (type == Type.TABLET || isTitleless) return

        titleView?.apply {
            scaleX = scale
            scaleY = scale
        }
    }

    private fun setIconScale(scale: Float) {
        if (isTitleless && type == Type.SHIFTING) {
            iconView?.apply {
                scaleX = scale
                scaleY = scale
            }
        }
    }

    public override fun onSaveInstanceState(): Parcelable? {
        if (badge != null) {
            return saveState().apply {
                putParcelable("superstate", super.onSaveInstanceState())
            }
        }

        return super.onSaveInstanceState()
    }

    @VisibleForTesting
    internal fun saveState(): Bundle {
        return Bundle().apply {
            putString(STATE_BADGE_LABEL + indexInTabContainer, badge?.label ?: "")
        }
    }

    public override fun onRestoreInstanceState(state: Parcelable?) {
        var stateVar = state

        (stateVar as? Bundle)?.let {
            restoreState(it)
            stateVar = it.getParcelable("superstate")
        }

        super.onRestoreInstanceState(stateVar)
    }

    @VisibleForTesting
    internal fun restoreState(savedInstanceState: Bundle) {
        badgeLabel = savedInstanceState.getString(STATE_BADGE_LABEL + indexInTabContainer)
    }

    internal enum class Type {
        FIXED, SHIFTING, TABLET
    }

    class Config private constructor(builder: Builder) {
        internal val inActiveTabAlpha: Float = builder.inActiveTabAlpha
        internal val activeTabAlpha: Float = builder.activeTabAlpha
        internal val inActiveTabColor: Int = builder.inActiveTabColor
        internal val activeTabColor: Int = builder.activeTabColor
        internal val barColorWhenSelected: Int = builder.barColorWhenSelected
        internal val badgeBackgroundColor: Int = builder.badgeBackgroundColor
        internal val badgeFontColor: Int = builder.badgeFontColor
        internal val titleTextAppearance: Int = builder.titleTextAppearance
        internal val titleTypeFace: Typeface? = builder.titleTypeFace
        internal val badgeHidesWhenSelected = builder.hidesBadgeWhenSelected

        class Builder {
            internal var inActiveTabAlpha: Float = 0F
            internal var activeTabAlpha: Float = 0F
            internal var inActiveTabColor: Int = 0
            internal var activeTabColor: Int = 0
            internal var barColorWhenSelected: Int = 0
            internal var badgeBackgroundColor: Int = 0
            internal var badgeFontColor: Int = 0
            internal var hidesBadgeWhenSelected = true
            internal var titleTextAppearance: Int = 0
            internal var titleTypeFace: Typeface? = null

            fun inActiveTabAlpha(alpha: Float): Builder =
                this.apply {
                    inActiveTabAlpha = alpha
                }

            fun activeTabAlpha(alpha: Float): Builder =
                this.apply {
                    activeTabAlpha = alpha
                }

            fun inActiveTabColor(@ColorInt color: Int): Builder =
                this.apply {
                    inActiveTabColor = color
                }

            fun activeTabColor(@ColorInt color: Int): Builder =
                this.apply {
                    activeTabColor = color
                }

            fun barColorWhenSelected(@ColorInt color: Int): Builder =
                this.apply {
                    barColorWhenSelected = color
                }

            fun badgeBackgroundColor(@ColorInt color: Int): Builder =
                this.apply {
                    badgeBackgroundColor = color
                }

            fun badgeFontColor(@ColorInt color: Int): Builder =
                this.apply {
                    badgeFontColor = color
                }

            fun hideBadgeWhenSelected(hide: Boolean): Builder =
                this.apply {
                    hidesBadgeWhenSelected = hide
                }

            fun titleTextAppearance(appearance: Int): Builder =
                this.apply {
                    titleTextAppearance = appearance
                }

            fun titleTypeFace(typeFace: Typeface?): Builder {
                typeFace ?: return this

                return this.apply {
                    titleTypeFace = typeFace
                }
            }

            fun build(): Config = Config(this)
        }
    }

    companion object {
        @VisibleForTesting
        internal val STATE_BADGE_LABEL = "STATE_BADGE_LABEL_FOR_TAB_"

        private const val ANIMATION_DURATION: Long = 150
        private const val ACTIVE_TITLE_SCALE = 1F
        private const val INACTIVE_FIXED_TITLE_SCALE = 0.86F
        private const val ACTIVE_SHIFTING_TITLELESS_ICON_SCALE = 1.24F
        private const val INACTIVE_SHIFTING_TITLELESS_ICON_SCALE = 1F
    }
}