package com.olekdia.bottombar

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.TargetApi
import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.util.AttributeSet
import android.view.View
import android.view.ViewAnimationUtils
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.*
import androidx.core.view.MarginLayoutParamsCompat
import androidx.core.view.ViewCompat
import androidx.core.view.ViewPropertyAnimatorListenerAdapter
import com.olekdia.androidcommon.NO_RESOURCE
import com.olekdia.androidcommon.extensions.*
import com.olekdia.common.extensions.*
import com.olekdia.common.INVALID
import kotlin.math.min
import kotlin.math.roundToInt

/*
 * BottomBar library for Android
 * Copyright (c) 2016 Iiro Krankka (http://github.com/roughike).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class BottomBar @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = NO_RESOURCE
) : LinearLayout(context, attrs, defStyleAttr),
    View.OnClickListener,
    View.OnLongClickListener {

    private var batchPropertyApplier: BatchTabPropertyApplier? = null
    private var primaryColor: Int = context.resolveColor(R.attr.colorPrimary)
    private var screenWidthDp: Int = context.resources.screenWidthDp.toInt()
    private var maxFixedItemWidth: Int = context.getDimensionPixelSize(R.dimen.bb_max_fixed_item_width)

    // XML Attributes
    private val tabXmlResource: Int
    private val behaviors: Int
    private var inActiveTabAlpha: Float = 0F
    private var activeTabAlpha: Float = 0F
    private var inActiveTabColor: Int = 0
    private var activeTabColor: Int = 0
    private var badgeBackgroundColor: Int = 0
    private var hideBadgeWhenActive: Boolean = false
    private var longPressHintsEnabled: Boolean = false
    private var titleTextAppearance: Int = 0
    private var titleTypeFace: Typeface? = null
    private val showShadow: Boolean
    private val fabAnchor: FabAnchor

    private var backgroundOverlay: View? = null
    private var outerContainer: ViewGroup? = null
    private var tabContainer: BottomBarTabContainer? = null

    private var defaultBackgroundColor: Int = Color.WHITE
    private var currentBackgroundColor: Int = 0
    /**
     * Get the currently selected tab position.
     */
    var currentTabPosition: Int = 0

    private var inActiveShiftingItemWidth: Int = 0
    private var activeShiftingItemWidth: Int = 0

    /**
     * Listener that gets fired when the selected [BottomBarTab] is about to change.
     */
    var tabSelectionInterceptor: TabSelectionInterceptor? = null
    /**
     * Listener that gets fired when the selected [BottomBarTab] changes.
     */
    var tabSelectListener: OnTabSelectListener? = null
    /**
     * Listener that gets fired when a currently selected [BottomBarTab] is clicked.
     */
    var tabReselectListener: OnTabReselectListener? = null

    private var isComingFromRestoredState: Boolean = false

    private lateinit var currentTabs: Array<BottomBarTab?>

    var isTabletMode: Boolean = false
        private set

    val isShiftingMode: Boolean
        get() = !isTabletMode && hasBehavior(BEHAVIOR_SHIFTING)

    val isIconsOnlyMode: Boolean
        get() = !isTabletMode && hasBehavior(BEHAVIOR_ICONS_ONLY)

    val tabConfig: BottomBarTab.Config
        get() = BottomBarTab.Config.Builder()
            .inActiveTabAlpha(inActiveTabAlpha)
            .activeTabAlpha(activeTabAlpha)
            .inActiveTabColor(inActiveTabColor)
            .activeTabColor(activeTabColor)
            .barColorWhenSelected(defaultBackgroundColor)
            .badgeBackgroundColor(badgeBackgroundColor)
            .badgeFontColor(Color.WHITE)
            .hideBadgeWhenSelected(hideBadgeWhenActive)
            .titleTextAppearance(titleTextAppearance)
            .titleTypeFace(titleTypeFace)
            .build()

    val tabCount: Int
        get() = tabContainer?.childCount ?: INVALID

    /**
     * Get the currently selected tab.
     */
    val currentTab: BottomBarTab?
        get() = getTabAtPosition(currentTabPosition)

    /**
     * Get the resource id for the currently selected tab.
     */
    val currentTabId: Int
        @IdRes
        get() = currentTab?.id ?: INVALID

    init {
        val ta: TypedArray = context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.BottomBar,
            defStyleAttr,
            NO_RESOURCE
        )

        try {
            tabXmlResource = ta.getResourceId(R.styleable.BottomBar_bb_tabXmlResource, 0)
            isTabletMode = ta.getBoolean(R.styleable.BottomBar_bb_tabletMode, false)
            behaviors = ta.getInteger(R.styleable.BottomBar_bb_behavior, BEHAVIOR_NONE)

            @ColorInt
            val defaultInActiveColor: Int =
                if (isShiftingMode) {
                    Color.WHITE
                } else {
                    context.getColorCompat(R.color.bb_inActiveBottomBarItemColor)
                }

            val defaultActiveColor: Int = if (isShiftingMode) Color.WHITE else primaryColor

            longPressHintsEnabled =
                ta.getBoolean(R.styleable.BottomBar_bb_longPressHintsEnabled, true)
            inActiveTabColor =
                ta.getColor(R.styleable.BottomBar_bb_inActiveTabColor, defaultInActiveColor)
            activeTabColor =
                ta.getColor(R.styleable.BottomBar_bb_activeTabColor, defaultActiveColor)
            badgeBackgroundColor =
                ta.getColor(R.styleable.BottomBar_bb_badgeBackgroundColor, Color.RED)

            inActiveTabAlpha = ta.getFloat(
                R.styleable.BottomBar_bb_inActiveTabAlpha,
                inActiveTabColor.alpha.toFloatColorComponent()
            )
            activeTabAlpha = ta.getFloat(
                R.styleable.BottomBar_bb_activeTabAlpha,
                activeTabColor.alpha.toFloatColorComponent()
            )
            activeTabColor = activeTabColor.toOpaqueColor()
            inActiveTabColor = inActiveTabColor.toOpaqueColor()

            hideBadgeWhenActive = ta.getBoolean(R.styleable.BottomBar_bb_badgesHideWhenActive, true)
            titleTextAppearance =
                ta.getResourceId(R.styleable.BottomBar_bb_titleTextAppearance, 0)
            titleTypeFace = ta.getTypeface(context, R.styleable.BottomBar_bb_titleTypeFace)
            showShadow = ta.getBoolean(R.styleable.BottomBar_bb_showShadow, true)

            fabAnchor = ta.getEnum(R.styleable.BottomBar_bb_fabAnchor, FabAnchor.NONE)
        } finally {
            ta.recycle()
        }

        batchPropertyApplier = BatchTabPropertyApplier(this)

        initializeViews()
        determineInitialBackgroundColor()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            init21()
        }

        if (tabXmlResource != 0) {
            setItems(tabXmlResource)
        }

        isFocusable = false
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun init21() {
        if (showShadow) {
            val shadowElevation: Int =
                resources.getDimensionPixelSize(R.dimen.bb_default_elevation)

            elevation = shadowElevation.toFloat()
            outlineProvider = ViewOutlineProvider.BOUNDS
        }
    }

    private fun hasBehavior(behavior: Int): Boolean = behaviors or behavior == behaviors

    private fun initializeViews() {
        val width: Int = if (isTabletMode) LayoutParams.WRAP_CONTENT else LayoutParams.MATCH_PARENT
        val height: Int = if (isTabletMode) LayoutParams.MATCH_PARENT else LayoutParams.WRAP_CONTENT
        val params = LayoutParams(width, height)

        layoutParams = params
        orientation = if (isTabletMode) HORIZONTAL else VERTICAL

        val rootView: View = View.inflate(
            context,
            if (isTabletMode) {
                R.layout.bb_bottom_bar_item_container_tablet
            } else {
                R.layout.bb_bottom_bar_item_container
            },
            this
        )
        rootView.layoutParams = params

        backgroundOverlay = rootView.findViewById(R.id.bb_bottom_bar_background_overlay)
        outerContainer = rootView.findViewById(R.id.bb_bottom_bar_outer_container)
        tabContainer = rootView.findViewById(R.id.bb_bottom_bar_item_container)

        if (!isTabletMode) {
            val containerParams: MarginLayoutParams =
                tabContainer?.layoutParams as? MarginLayoutParams ?: return

            when (fabAnchor) {
                FabAnchor.NONE -> {}

                FabAnchor.START ->
                    MarginLayoutParamsCompat.setMarginStart(
                        containerParams,
                        context.resources.getDimensionPixelSize(R.dimen.bb_fab_margin)
                    )

                FabAnchor.END ->
                    MarginLayoutParamsCompat.setMarginEnd(
                        containerParams,
                        context.resources.getDimensionPixelSize(R.dimen.bb_fab_margin)
                    )
            }

            tabContainer?.layoutParams = containerParams
        }
    }

    private fun determineInitialBackgroundColor() {
        if (isShiftingMode) {
            defaultBackgroundColor = primaryColor
        }

        val userDefinedBackground: Drawable = background ?: return

        val userHasDefinedBackgroundColor: Boolean = userDefinedBackground is ColorDrawable

        if (userHasDefinedBackgroundColor) {
            defaultBackgroundColor = (userDefinedBackground as ColorDrawable).color
            setBackgroundColor(Color.TRANSPARENT)
        }
    }

    /**
     * Set the item for the BottomBar from XML Resource with a default configuration
     * for each tab.
     */
    @JvmOverloads
    fun setItems(@XmlRes xmlRes: Int, defaultTabConfig: BottomBarTab.Config? = null) {
        if (xmlRes == 0) throw RuntimeException("No items specified for the BottomBar!")

        val defTagConf: BottomBarTab.Config = defaultTabConfig ?: tabConfig
        val parser = TabParser(context, defTagConf, xmlRes)

        updateItems(parser.parseTabs())
    }

    fun updateItems(bottomBarItems: List<BottomBarTab>) {
        tabContainer?.removeAllViews()

        var biggestWidth = 0

        val viewsToAdd: Array<BottomBarTab?> = arrayOfNulls(bottomBarItems.size)

        for ((index, bottomBarTab) in bottomBarItems.withIndex()) {
            val type: BottomBarTab.Type =
                when {
                    isShiftingMode -> BottomBarTab.Type.SHIFTING
                    isTabletMode -> BottomBarTab.Type.TABLET
                    else -> BottomBarTab.Type.FIXED
                }

            if (isIconsOnlyMode) {
                bottomBarTab.isTitleless = true
            }

            bottomBarTab.type = type
            bottomBarTab.prepareLayout()

            if (index == currentTabPosition) {
                bottomBarTab.select(false)
                handleBackgroundColorChange(bottomBarTab, false)
            } else {
                bottomBarTab.deselect(false)
            }

            if (!isTabletMode) {
                if (bottomBarTab.width > biggestWidth) {
                    biggestWidth = bottomBarTab.width
                }

                viewsToAdd[index] = bottomBarTab
            } else {
                tabContainer?.addView(bottomBarTab)
            }

            bottomBarTab.setOnClickListener(this)
            bottomBarTab.setOnLongClickListener(this)
        }

        currentTabs = viewsToAdd

        if (!isTabletMode) {
            resizeTabsToCorrectSizes(viewsToAdd)
        }
    }

    private fun resizeTabsToCorrectSizes(tabsToAdd: Array<BottomBarTab?>) {
        val res = context.resources
        var viewWidthDp: Int = res.pxToDp(width).roundToInt()

        if (viewWidthDp <= 0 || viewWidthDp > screenWidthDp) {
            viewWidthDp = screenWidthDp
        }

        val proposedItemWidth: Int =
            min(res.dpToPx((viewWidthDp - 56) / tabsToAdd.size), maxFixedItemWidth)

        inActiveShiftingItemWidth = (proposedItemWidth * 0.9).toInt()
        activeShiftingItemWidth =
            (proposedItemWidth + proposedItemWidth * ((tabsToAdd.size - 1) * 0.1)).toInt()

        val tabHeight: Int = res.getDimension(R.dimen.bb_height).roundToInt()
        var tabWidth: Int

        var tabView: BottomBarTab
        var params: ViewGroup.LayoutParams
        var isParamsChanged: Boolean

        for (i in tabsToAdd.indices) {
            tabView = tabsToAdd[i] ?: continue
            params = tabView.layoutParams

            tabWidth =
                if (isShiftingMode) {
                    if (tabView.isActive) activeShiftingItemWidth else inActiveShiftingItemWidth
                } else {
                    proposedItemWidth
                }

            if (params.height != tabHeight || params.width != tabWidth) {
                params.apply {
                    height = tabHeight
                    width = tabWidth
                }
                isParamsChanged = true
            } else {
                isParamsChanged = false
            }

            if (tabView.parent == null) {
                tabContainer?.let {
                    if (ViewCompat.isInLayout(this)) {
                        it.addViewInLayout(tabView, i, params)
                    } else {
                        it.addView(tabView, i, params)
                    }
                }
            } else if (isParamsChanged) {
                tabView.layoutParams = params
            }
        }
    }

    /**
     * Set a listener that gets fired when the selected [BottomBarTab] is about to change.
     *
     * @param interceptor a listener for potentially interrupting changes in tab selection.
     */
    fun shouldInterceptTabSelection(interceptor: TabSelectionInterceptor) {
        tabSelectionInterceptor = interceptor
    }

    /**
     * Set a listener that gets fired when the selected [BottomBarTab] changes.
     *
     * @param listener a listener for handling tab selections.
     */
    fun onTabSelected(listener: OnTabSelectListener) {
        tabSelectListener = listener
    }

    /**
     * Set a listener that gets fired when a currently selected [BottomBarTab] is clicked.
     *
     * @param listener a listener for handling tab reselections.
     */
    fun onTabReSelected(listener: OnTabReselectListener) {
        tabReselectListener = listener
    }

    /**
     * Set the default selected to be the tab with the corresponding tab id.
     * By default, the first tab in the container is the default tab.
     */
    fun setDefaultTab(@IdRes defaultTabId: Int) {
        findPositionForTabWithId(defaultTabId)?.let {
            setDefaultTabPosition(it)
        }
    }

    /**
     * Sets the default tab for this BottomBar that is shown until the user changes
     * the selection.
     *
     * @param defaultTabPosition the default tab position.
     */
    fun setDefaultTabPosition(defaultTabPosition: Int) {
        if (isComingFromRestoredState) return

        selectTabAtPosition(defaultTabPosition)
    }

    /**
     * Select the tab with the corresponding id.
     */
    fun selectTabWithId(@IdRes tabResId: Int) {
        findPositionForTabWithId(tabResId)?.let {
            selectTabAtPosition(it)
        }
    }

    /**
     * Select a tab at the specified position.
     *
     * @param position the position to select.
     * @param animate  should the tab change be animated or not.
     */
    @JvmOverloads
    fun selectTabAtPosition(position: Int, animate: Boolean = false) {
        if (position > tabCount - 1 || position < 0) {
            throw IndexOutOfBoundsException(
                "Can't select tab at position " +
                        position +
                        ". This BottomBar has no items at that position."
            )
        }

        val oldTab: BottomBarTab = currentTab ?: return
        val newTab: BottomBarTab = getTabAtPosition(position) ?: return

        oldTab.deselect(animate)
        newTab.select(animate)

        updateSelectedTab(position)
        shiftingMagic(oldTab, newTab, animate)
        handleBackgroundColorChange(newTab, animate)
    }

    /**
     * Get the tab at the specified position.
     */
    fun getTabAtPosition(position: Int): BottomBarTab? {
        val child: View? = tabContainer?.getChildAt(position)

        return (child as? BadgeContainer)?.let {
            findTabInLayout(it)
        } ?: child as? BottomBarTab
    }

    /**
     * Find the tabs' position in the container by id.
     */
    fun findPositionForTabWithId(@IdRes tabId: Int): Int? = getTabWithId(tabId)?.indexInTabContainer

    /**
     * Find a BottomBarTab with the corresponding id.
     */
    fun getTabWithId(@IdRes tabId: Int): BottomBarTab? =
        tabContainer?.findViewById<View>(tabId) as? BottomBarTab

    /**
     * Controls whether the long pressed tab title should be displayed in
     * a helpful Toast if the title is not currently visible.
     *
     * @param enabled true if toasts should be shown to indicate the title
     * of a long pressed tab, false otherwise.
     */
    fun setLongPressHintsEnabled(enabled: Boolean) {
        longPressHintsEnabled = enabled
    }

    /**
     * Set alpha value used for inactive BottomBarTabs.
     */
    fun setInActiveTabAlpha(alpha: Float) {
        inActiveTabAlpha = alpha

        batchPropertyApplier?.applyToAllTabs(
            object : BatchTabPropertyApplier.TabPropertyUpdater {
                override fun update(tab: BottomBarTab) {
                    tab.inActiveAlpha = inActiveTabAlpha
                }
            })
    }

    /**
     * Set alpha value used for active BottomBarTabs.
     */
    fun setActiveTabAlpha(alpha: Float) {
        activeTabAlpha = alpha

        batchPropertyApplier?.applyToAllTabs(
            object : BatchTabPropertyApplier.TabPropertyUpdater {
                override fun update(tab: BottomBarTab) {
                    tab.activeAlpha = activeTabAlpha
                }
            })
    }

    fun setInActiveTabColor(@ColorInt color: Int) {
        inActiveTabColor = color

        batchPropertyApplier?.applyToAllTabs(
            object : BatchTabPropertyApplier.TabPropertyUpdater {
                override fun update(tab: BottomBarTab) {
                    tab.inActiveColor = inActiveTabColor
                }
            })
    }

    /**
     * Set active color used for selected BottomBarTabs.
     */
    fun setActiveTabColor(@ColorInt color: Int) {
        activeTabColor = color

        batchPropertyApplier?.applyToAllTabs(
            object : BatchTabPropertyApplier.TabPropertyUpdater {
                override fun update(tab: BottomBarTab) {
                    tab.activeColor = activeTabColor
                }
            })
    }

    /**
     * Set background color for the badge.
     */
    fun setBadgeBackgroundColor(@ColorInt color: Int) {
        badgeBackgroundColor = color

        batchPropertyApplier?.applyToAllTabs(
            object : BatchTabPropertyApplier.TabPropertyUpdater {
                override fun update(tab: BottomBarTab) {
                    tab.badgeBackgroundColor = badgeBackgroundColor
                }
            })
    }

    /**
     * Controls whether the badge (if any) for active tabs
     * should be hidden or not.
     */
    fun setBadgesHideWhenActive(hideWhenSelected: Boolean) {
        hideBadgeWhenActive = hideWhenSelected

        batchPropertyApplier?.applyToAllTabs(
            object : BatchTabPropertyApplier.TabPropertyUpdater {
                override fun update(tab: BottomBarTab) {
                    tab.badgeHidesWhenActive = hideWhenSelected
                }
            })
    }

    /**
     * Set custom text apperance for all BottomBarTabs.
     */
    fun setTabTitleTextAppearance(textAppearance: Int) {
        titleTextAppearance = textAppearance

        batchPropertyApplier?.applyToAllTabs(
            object : BatchTabPropertyApplier.TabPropertyUpdater {
                override fun update(tab: BottomBarTab) {
                    tab.titleTextAppearance = titleTextAppearance
                }
            })
    }

    /**
     * Set a custom typeface for all tab's titles.
     *
     * @param fontName name of your custom font file.
     * In that case your font path would look like src/main/assets/fonts/MySuperDuperFont.ttf,
     * but you only need to provide MySuperDuperFont.
     */
    fun setTabTitleTypeface(fontName: String) {
        val actualTypeface: Typeface? = context.getTypeface(fontName)
        setTabTitleTypeface(actualTypeface)
    }

    /**
     * Set a custom typeface for all tab's titles.
     */
    fun setTabTitleTypeface(typeface: Typeface?) {
        titleTypeFace = typeface

        batchPropertyApplier?.applyToAllTabs(
            object : BatchTabPropertyApplier.TabPropertyUpdater {
                override fun update(tab: BottomBarTab) {
                    tab.setTitleTypeface(titleTypeFace)
                }
            })
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

        if (changed) {
            if (!isTabletMode) {
                resizeTabsToCorrectSizes(currentTabs)
            }
        }
    }

    public override fun onSaveInstanceState(): Parcelable? =
        saveState().apply {
            putParcelable("superstate", super.onSaveInstanceState())
        }

    @VisibleForTesting
    internal fun saveState(): Bundle =
        Bundle().apply {
            putInt(STATE_CURRENT_SELECTED_TAB, currentTabId)
        }

    public override fun onRestoreInstanceState(state: Parcelable?) {
        var stateVar: Parcelable? = state

        (stateVar as? Bundle)?.let {
            restoreState(it)
            stateVar = it.getParcelable("superstate")
        }

        super.onRestoreInstanceState(stateVar)
    }

    @VisibleForTesting
    internal fun restoreState(savedInstanceState: Bundle?) {
        val state: Bundle = savedInstanceState ?: return

        isComingFromRestoredState = true

        let(
            state.getInt(STATE_CURRENT_SELECTED_TAB, INVALID), tabReselectListener
        ) { restoredId, reselectListener ->
            tabReselectListener = null
            selectTabWithId(restoredId)
            tabReselectListener = reselectListener
        }
    }

    override fun onClick(target: View) {
        val tab: BottomBarTab = target as? BottomBarTab ?: return
        handleClick(tab)
    }

    override fun onLongClick(target: View): Boolean {
        val tab: BottomBarTab = target as? BottomBarTab ?: return false
        return handleLongClick(tab)
    }

    private fun findTabInLayout(child: ViewGroup): BottomBarTab? {
        for (i in 0 until child.childCount) {
            val candidate: View = child.getChildAt(i)

            return candidate as? BottomBarTab ?: continue
        }

        return null
    }

    private fun handleClick(newTab: BottomBarTab) {
        val oldTab: BottomBarTab? = currentTab

        tabSelectionInterceptor?.let {
            if (it.shouldInterceptTabSelection(oldTab?.id ?: INVALID, newTab.id)) return
        }

        oldTab?.deselect(true)
        newTab.select(true)

        oldTab?.let {
            shiftingMagic(it, newTab, true)
        }
        handleBackgroundColorChange(newTab, true)
        updateSelectedTab(newTab.indexInTabContainer)
    }

    private fun handleLongClick(longClickedTab: BottomBarTab): Boolean {
        val areInactiveTitlesHidden: Boolean = isShiftingMode || isTabletMode
        val isClickedTitleHidden: Boolean = !longClickedTab.isActive
        val shouldShowHint: Boolean =
            areInactiveTitlesHidden && isClickedTitleHidden && longPressHintsEnabled

        if (shouldShowHint) {
            Toast
                .makeText(context, longClickedTab.title, Toast.LENGTH_SHORT)
                .show()
        }

        return true
    }

    private fun updateSelectedTab(newPosition: Int) {
        val newTabId: Int? = getTabAtPosition(newPosition)?.id

        newTabId?.also {
            if (newPosition != currentTabPosition) {
                tabSelectListener?.onTabSelected(it)
            } else {
                tabReselectListener?.onTabReSelected(it)
            }
        }

        currentTabPosition = newPosition
    }

    private fun shiftingMagic(oldTab: BottomBarTab, newTab: BottomBarTab, animate: Boolean) {
        if (isShiftingMode) {
            oldTab.updateWidth(inActiveShiftingItemWidth.toFloat(), animate)
            newTab.updateWidth(activeShiftingItemWidth.toFloat(), animate)
        }
    }

    private fun handleBackgroundColorChange(tab: BottomBarTab, animate: Boolean) {
        val newColor: Int = tab.barColorWhenSelected

        if (currentBackgroundColor == newColor) return

        if (!animate) {
            outerContainer?.setBackgroundColor(newColor)
            return
        }

        var clickedView: View = tab

        if (tab.hasActiveBadge()) {
            tab.outerView?.let {
                clickedView = it
            }
        }

        animateBGColorChange(clickedView, newColor)
        currentBackgroundColor = newColor
    }

    private fun animateBGColorChange(clickedView: View, newColor: Int) {
        prepareForBackgroundColorAnimation(newColor)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (outerContainer?.isAttachedToWindow == false) {
                return
            }

            backgroundCircularRevealAnimation(clickedView, newColor)
        } else {
            backgroundCrossfadeAnimation(newColor)
        }
    }

    private fun prepareForBackgroundColorAnimation(newColor: Int) {
        outerContainer?.clearAnimation()

        backgroundOverlay?.apply {
            clearAnimation()
            setBackgroundColor(newColor)
            visibility = View.VISIBLE
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun backgroundCircularRevealAnimation(clickedView: View, newColor: Int) {
        val centerX: Int = (clickedView.x + clickedView.measuredWidth / 2).toInt()
        val yOffset: Int = if (isTabletMode) clickedView.y.toInt() else 0
        val centerY: Int = yOffset + clickedView.measuredHeight / 2
        val startRadius = 0F
        val finalRadius: Int =
            (if (isTabletMode) outerContainer?.height else outerContainer?.width) ?: 0

        val animator = ViewAnimationUtils.createCircularReveal(
            backgroundOverlay,
            centerX,
            centerY,
            startRadius,
            finalRadius.toFloat()
        )

        if (isTabletMode) {
            animator.duration = 500L
        }

        animator.addListener(
            object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    onEnd()
                }

                override fun onAnimationCancel(animation: Animator) {
                    onEnd()
                }

                private fun onEnd() {
                    outerContainer?.setBackgroundColor(newColor)

                    backgroundOverlay?.apply {
                        visibility = View.INVISIBLE
                        alpha = 1F
                    }
                }
            })

        animator.start()
    }

    private fun backgroundCrossfadeAnimation(newColor: Int) {
        val overlay: View = backgroundOverlay ?: return

        overlay.alpha = 0F

        ViewCompat
            .animate(overlay)
            .alpha(1F)
            .setListener(
                object : ViewPropertyAnimatorListenerAdapter() {
                    override fun onAnimationEnd(view: View) {
                        onEnd()
                    }

                    override fun onAnimationCancel(view: View) {
                        onEnd()
                    }

                    private fun onEnd() {
                        outerContainer?.setBackgroundColor(newColor)

                        overlay.apply {
                            visibility = View.INVISIBLE
                            alpha = 1F
                        }
                    }
                })
            .start()
    }

    enum class FabAnchor {
        NONE, START, END
    }

    companion object {
        private const val STATE_CURRENT_SELECTED_TAB = "STATE_CURRENT_SELECTED_TAB"
        const val AVG_NUMBER_OF_TABS = 5
        // Behaviors
        private const val BEHAVIOR_NONE = 0
        private const val BEHAVIOR_SHIFTING = 1
        private const val BEHAVIOR_ICONS_ONLY = 2
    }
}