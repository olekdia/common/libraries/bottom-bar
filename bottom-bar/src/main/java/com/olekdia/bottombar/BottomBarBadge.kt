package com.olekdia.bottombar

import android.content.Context
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import android.os.Build
import android.view.Gravity
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.ViewCompat
import com.olekdia.androidcommon.extensions.*
import kotlin.math.max

/*
 * BottomBar library for Android
 * Copyright (c) 2016 Iiro Krankka (http://github.com/roughike).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
internal class BottomBarBadge(context: Context) : AppCompatTextView(context) {

    /**
     * Get the currently showing label for this Badge.
     *
     * @return current label for the Badge.
     */
    /**
     * Set the unread / new item / whatever label for this Badge.
     *
     * @param label the value this Badge should show.
     */
    var label: String? = null
        set(label) {
            field = label
            text = label
        }
    /**
     * Is this badge currently visible?
     *
     * @return true is this badge is visible, otherwise false.
     */
    var isVisible = true
        private set

    private val translY: Float = context.resources.dpToPx(5f)

    /**
     * Shows the badge with a neat little scale animation.
     */
    fun show() {
        isVisible = true
        ViewCompat
            .animate(this)
            .setDuration(150)
            .alpha(1F)
            .scaleX(1F)
            .scaleY(1F)
            .start()
    }

    /**
     * Hides the badge with a neat little scale animation.
     */
    fun hide() {
        isVisible = false
        ViewCompat
            .animate(this)
            .setDuration(150)
            .alpha(0F)
            .scaleX(0F)
            .scaleY(0F)
            .start()
    }

    fun attachToTab(tab: BottomBarTab, backgroundColor: Int, fontColor: Int) {
        val params: FrameLayout.LayoutParams =
            FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT
            )

        if (tab.type == BottomBarTab.Type.TABLET) {
            params.gravity = Gravity.CENTER_HORIZONTAL
        }

        layoutParams = params
        gravity = Gravity.CENTER

        setTextAppearanceCompat(R.style.BB_BottomBarBadge_Text)

        setColoredCircleBackground(backgroundColor)
        setFontColor(fontColor)
        wrapTabAndBadgeInSameContainer(tab)
    }

    fun setFontColor(color: Int) {
        setTextColor(color)
    }

    fun setColoredCircleBackground(circleColor: Int) {
        val backgroundCircle = ShapeDrawable(OvalShape())
        backgroundCircle.paint.color = circleColor

        setPadding(0, 0, 0, 0)

        backgroundCompat = backgroundCircle
    }

    private fun wrapTabAndBadgeInSameContainer(tab: BottomBarTab) {
        val tabContainer: ViewGroup? = tab.parent as? ViewGroup
        tabContainer?.removeView(tab)

        val badgeContainer = BadgeContainer(context)

        badgeContainer.layoutParams =
            LinearLayout.LayoutParams(
                if (tab.type == BottomBarTab.Type.TABLET) {
                    ViewGroup.LayoutParams.MATCH_PARENT
                } else {
                    ViewGroup.LayoutParams.WRAP_CONTENT
                },
                ViewGroup.LayoutParams.WRAP_CONTENT
            )

        tryAdjustTabParams(tab, true)
        badgeContainer.addView(tab)
        badgeContainer.addView(this)

        tabContainer?.addView(badgeContainer, tab.indexInTabContainer)
        badgeContainer.performOnLayout {
            adjustPositionAndSize(tab)
        }
    }

    fun removeFromTab(tab: BottomBarTab) {
        val badgeAndTabContainer: BadgeContainer = parent as? BadgeContainer ?: return
        val originalTabContainer: ViewGroup = badgeAndTabContainer.parent as? ViewGroup ?: return

        badgeAndTabContainer.removeView(tab)
        originalTabContainer.removeView(badgeAndTabContainer)
        tryAdjustTabParams(tab, false)
        originalTabContainer.addView(tab, tab.indexInTabContainer)
    }

    private fun tryAdjustTabParams(tab: BottomBarTab, forBadgeContainer: Boolean) {
        if (tab.type == BottomBarTab.Type.TABLET) {
            if (forBadgeContainer) {
                val params: FrameLayout.LayoutParams =
                    FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                params.gravity = Gravity.CENTER_HORIZONTAL
                tab.layoutParams = params
            } else {
                val params: LinearLayout.LayoutParams =
                    LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                params.gravity = Gravity.CENTER_HORIZONTAL
                tab.layoutParams = params
            }
        }
    }

    fun adjustPositionAndSize(tab: BottomBarTab) {
        val iconView: AppCompatImageView? = tab.iconView
        val params: ViewGroup.MarginLayoutParams? = layoutParams as? ViewGroup.MarginLayoutParams

        val size: Int = max(width, height)

        iconView?.let {
            if (tab.type == BottomBarTab.Type.TABLET) {
                translationX = it.width * 0.6F
            } else {
                x = it.x + it.width / 1.25F
            }
        }

        translationY = translY

        params?.let {
            if (it.width != size || it.height != size) {
                it.width = size
                it.height = size
                layoutParams = it
            }
        }
    }
}

class BadgeContainer(context: Context) : FrameLayout(context) {
    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            layoutDirection = LAYOUT_DIRECTION_LTR
        }
    }
}