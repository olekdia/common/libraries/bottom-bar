package com.olekdia.bottombar

import android.content.Context
import android.content.res.XmlResourceParser
import android.graphics.Color
import androidx.annotation.*
import androidx.annotation.IntRange
import androidx.core.content.ContextCompat
import com.olekdia.androidcommon.NO_RESOURCE
import com.olekdia.bottombar.TabParser.TabAttribute.Companion.ACTIVE_COLOR
import com.olekdia.bottombar.TabParser.TabAttribute.Companion.BADGE_BACKGROUND_COLOR
import com.olekdia.bottombar.TabParser.TabAttribute.Companion.BADGE_FONT_COLOR
import com.olekdia.bottombar.TabParser.TabAttribute.Companion.BADGE_HIDES_WHEN_ACTIVE
import com.olekdia.bottombar.TabParser.TabAttribute.Companion.BAR_COLOR_WHEN_SELECTED
import com.olekdia.bottombar.TabParser.TabAttribute.Companion.ICON
import com.olekdia.bottombar.TabParser.TabAttribute.Companion.ID
import com.olekdia.bottombar.TabParser.TabAttribute.Companion.INACTIVE_COLOR
import com.olekdia.bottombar.TabParser.TabAttribute.Companion.IS_TITLELESS
import com.olekdia.bottombar.TabParser.TabAttribute.Companion.TITLE
import com.olekdia.common.INVALID
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException
import java.util.*

/**
 * Created by iiro on 21.7.2016.
 *
 * BottomBar library for Android
 * Copyright (c) 2016 Iiro Krankka (http://github.com/roughike).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
internal class TabParser(
    private val context: Context,
    private val defaultTabConfig: BottomBarTab.Config,
    @XmlRes tabsXmlResId: Int
) {

    private val parser: XmlResourceParser = context.resources.getXml(tabsXmlResId)

    private var tabs: MutableList<BottomBarTab>? = null

    @CheckResult
    fun parseTabs(): List<BottomBarTab> =
        tabs
            ?: ArrayList<BottomBarTab>(BottomBar.AVG_NUMBER_OF_TABS).also {
                try {
                    var eventType: Int
                    do {
                        eventType = parser.next()

                        if (eventType == XmlResourceParser.START_TAG && TAB_TAG == parser.name) {
                            val bottomBarTab: BottomBarTab = parseNewTab(parser, it.size)
                            it.add(bottomBarTab)
                        }
                    } while (eventType != XmlResourceParser.END_DOCUMENT)

                    tabs = it

                    return it
                } catch (e: IOException) {
                    e.printStackTrace()
                    throw TabParserException()
                } catch (e: XmlPullParserException) {
                    e.printStackTrace()
                    throw TabParserException()
                }
            }

    private fun parseNewTab(
        parser: XmlResourceParser,
        @IntRange(from = 0) containerPosition: Int
    ): BottomBarTab {
        val workingTab: BottomBarTab = tabWithDefaults()
        workingTab.indexInTabContainer = containerPosition

        val numberOfAttributes: Int = parser.attributeCount

        loop@ for (i in 0 until numberOfAttributes) {
            @TabAttribute
            val attrName: String = parser.getAttributeName(i)

            when (attrName) {
                ID ->
                    workingTab.id = parser.getIdAttributeResourceValue(i)

                ICON ->
                    workingTab.iconResId = parser.getAttributeResourceValue(i, NO_RESOURCE)

                TITLE ->
                    workingTab.title = getTitleValue(parser, i)


                INACTIVE_COLOR -> {
                    val inactiveColor: Int = getColorValue(parser, i)
                    if (inactiveColor == INVALID) continue@loop

                    workingTab.inActiveColor = inactiveColor
                }

                ACTIVE_COLOR -> {
                    val activeColor: Int = getColorValue(parser, i)
                    if (activeColor == INVALID) continue@loop

                    workingTab.activeColor = activeColor
                }

                BAR_COLOR_WHEN_SELECTED -> {
                    val barColorWhenSelected: Int = getColorValue(parser, i)
                    if (barColorWhenSelected == INVALID) continue@loop

                    workingTab.barColorWhenSelected = barColorWhenSelected
                }

                BADGE_BACKGROUND_COLOR -> {
                    val badgeBackgroundColor: Int = getColorValue(parser, i)
                    if (badgeBackgroundColor == INVALID) continue@loop

                    workingTab.badgeBackgroundColor = badgeBackgroundColor
                }

                BADGE_FONT_COLOR -> {
                    val badgeFontColor: Int = getColorValue(parser, i)
                    if (badgeFontColor == INVALID) continue@loop

                    workingTab.badgeFontColor = badgeFontColor
                }

                BADGE_HIDES_WHEN_ACTIVE -> {
                    val badgeHidesWhenActive: Boolean = parser.getAttributeBooleanValue(i, true)

                    workingTab.badgeHidesWhenActive = badgeHidesWhenActive
                }

                IS_TITLELESS -> {
                    val isTitleless: Boolean = parser.getAttributeBooleanValue(i, false)

                    workingTab.isTitleless = isTitleless
                }
            }
        }

        return workingTab
    }

    private fun tabWithDefaults(): BottomBarTab =
        BottomBarTab(context).apply {
            setConfig(defaultTabConfig)
        }

    private fun getTitleValue(
        parser: XmlResourceParser,
        @IntRange(from = 0) attrIndex: Int
    ): String {
        val titleResource: Int = parser.getAttributeResourceValue(attrIndex, 0)

        return if (titleResource == NO_RESOURCE)
            parser.getAttributeValue(attrIndex)
        else
            context.getString(titleResource)
    }

    @ColorInt
    private fun getColorValue(parser: XmlResourceParser, @IntRange(from = 0) attrIndex: Int): Int {
        val colorResource: Int = parser.getAttributeResourceValue(attrIndex, 0)

        if (colorResource == NO_RESOURCE) {
            return try {
                val colorValue: String = parser.getAttributeValue(attrIndex)
                Color.parseColor(colorValue)
            } catch (ignored: Exception) {
                INVALID
            }
        }

        return ContextCompat.getColor(context, colorResource)
    }

    @kotlin.annotation.Retention(AnnotationRetention.SOURCE)
    @StringDef(
        ID,
        ICON,
        TITLE,
        INACTIVE_COLOR,
        ACTIVE_COLOR,
        BAR_COLOR_WHEN_SELECTED,
        BADGE_BACKGROUND_COLOR,
        BADGE_FONT_COLOR,
        BADGE_HIDES_WHEN_ACTIVE,
        IS_TITLELESS
    )
    internal annotation class TabAttribute {
        companion object {
            const val ID = "id"
            const val ICON = "icon"
            const val TITLE = "title"
            const val INACTIVE_COLOR = "inActiveColor"
            const val ACTIVE_COLOR = "activeColor"
            const val BAR_COLOR_WHEN_SELECTED = "barColorWhenSelected"
            const val BADGE_BACKGROUND_COLOR = "badgeBackgroundColor"
            const val BADGE_FONT_COLOR = "badgeFontColor"
            const val BADGE_HIDES_WHEN_ACTIVE = "badgeHidesWhenActive"
            const val IS_TITLELESS = "iconOnly"
        }
    }

    class TabParserException :
        RuntimeException()
    // This class is just to be able to have a type of Runtime Exception that will make it clear where the error originated.

    companion object {
        private const val TAB_TAG = "tab"
    }
}
