package com.olekdia.bottombar

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.olekdia.androidcommon.NO_RESOURCE

class BottomBarTabContainer @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = NO_RESOURCE
) : LinearLayout(context, attrs, defStyleAttr) {

    public override fun addViewInLayout(
        child: View,
        index: Int,
        params: ViewGroup.LayoutParams
    ): Boolean {
        return super.addViewInLayout(child, index, params)
    }
}
